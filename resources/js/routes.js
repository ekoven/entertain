import { createRouter, createWebHashHistory } from 'vue-router'

let routes = [
    {
        path: '/',
        component: require ('./App.vue')
    },
    {
        path: '/piece/:id',
        component: require('./components/Piece.vue')
    },
    {
        path: '/contact',
        component: require('./components/ExampleComponent.vue')
    }
];

const router = createRouter({
    routes: routes,
    history: createWebHashHistory(),
    linkActiveClass: 'is-active'
});

export default router;
