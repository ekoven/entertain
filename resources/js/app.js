require('./bootstrap.js');


import { createApp } from 'vue'
import App from './App.vue'
import ExampleComponent from './components/ExampleComponent.vue'
import Piece from './components/Piece.vue'
import router from './routes.js'


createApp({
    components: {
        App,
        Piece,
        ExampleComponent,
    },
}).use(router).mount('#app')
