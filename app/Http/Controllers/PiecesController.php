<?php

namespace App\Http\Controllers;

use App\Piece;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class PiecesController extends Controller
{
    // Methods
    public function create(Request $request)
    {
        $work_formats = array('eBook','Dead tree','Switch','PS4','PC','PS Vita','3DS','Legacy');

        $work = new Piece();
        $work->title = $request->title;
        $work->length = $request->length;
        $work->format = $request->format;
        $work->save();

        return response($work->jsonSerialize(), Response::HTTP_CREATED);
    }

    public function index()
    {
        return response(Piece::all()->jsonSerialize(), Response::HTTP_OK);
    }

    public function update(Request $request, $id)
    {
        $work = Piece::findOrFail($id);
        $work->title = $request->title;
        $work->length = $request->length;
        $work->format = $request->format;
        $work->pubdate = $request->pubdate;
        $work->save();

        return response($work->jsonSerialize(), Response::HTTP_OK);
    }

    public function destroy($id)
    {
        Piece::destroy($id);
        return response(null, Response::HTTP_OK);
    }
}
